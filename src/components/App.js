import React, { Component } from 'react';
import './App.css';
import Display from '../containers/Display';

export default class App extends Component {
  render() {
    return (
      <div className="App">
        <Display />
      </div>
    );
  }
}
