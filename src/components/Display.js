import React, { Component } from 'react';
import Toilet from '../containers/Toilet';

export default class Display extends Component {
  render() {
    return (
      <div className="Display">
        {this.props.toilets.map((toilet, index) => 
          <Toilet key={toilet.id} id={toilet.id} name={toilet.name} />)}
      </div>
    );
  }
}

