import React, { Component } from 'react';

export default class Toilet extends Component {
  render() {
    return (
      <div
        title={this.props.name}
        id={this.props.id}
        className={`Toilet ${this.props.state}`}>
      </div>
    );
  }
}


