import { combineReducers } from 'redux'
import display from './Display'
import toilet from './Toilet'

const App = combineReducers({
  display,
  toilet
})

export default App
