const defaultState = {
  states:{
    t00: 'offline',
    t01: 'offline',
    t02: 'offline',
    t03: 'offline',
    t04: 'offline',
    t05: 'offline',
    t06: 'offline',
    t08: 'offline'
  }
}


const toiletStatesMap = {
  busy: 'occupied',
  free: 'vacant',
  offline: 'offline',
  online: 'online'
}

// needed until we fix names in manager
const mapToiletStates = states => Object.keys(states).reduce(function(newStates, current) {
  newStates[current] = toiletStatesMap[states[current]];
  return newStates;
}, {});

const toilet = (state = defaultState, action) => {
  switch (action.type) {
    case 'UPDATE_TOILET_STATES':
      return {
        ...state,
        states: mapToiletStates(action.states)
      }
    default:
      return state
  }
}

export default toilet
