import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import registerServiceWorker from './registerServiceWorker'
import { Provider } from 'react-redux'
import { createStore } from 'redux'

import AppStore from './reducers'
import App from './components/App'
import {updateToiletStates} from './actions'

let store = createStore(AppStore)



var ws = new WebSocket("ws://10.93.1.197:5678/view");
ws.onmessage = event => {
  store.dispatch(updateToiletStates(event))
}

ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();
