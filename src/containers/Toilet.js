import { connect } from 'react-redux'
import { toggleTodo } from '../actions'
import ToiletComponent from '../components/Toilet'

const mapStateToProps = (state, props) => {
  return {
    state: state.toilet.states[props.id]
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: id => {
      dispatch(toggleTodo(id))
    }
  }
}

const ToiletContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ToiletComponent)

export default ToiletContainer
