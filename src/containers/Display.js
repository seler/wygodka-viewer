import { connect } from 'react-redux'
import { toggleTodo } from '../actions'
import DisplayComponent from '../components/Display'

const toilets = [
  {
    id: 't00',
    name: "Toaleta w piwnicy"
  },
  {
    id: 't01',
    name: "Taleta w lokalu 1"
  },
  {
    id: 't02',
    name: "Toaleta w lokalu 2"
  },
  {
    id: 't03',
    name: "Toaleta w lokalu 3"
  },
  {
    id: 't04',
    name: "Toaleta w lokalu 4"
  },
  {
    id: 't05',
    name: "Toaleta w lokalu 5"
  },
  {
    id: 't06',
    name: "Toaleta w lokalu 6"
  },
  {
    id: 't08',
    name: "Toaleta w lokalu 8"
  }
];

const mapStateToProps = state => {
  return {
    toilets
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onTodoClick: id => {
      dispatch(toggleTodo(id))
    }
  }
}

const DisplayContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(DisplayComponent)

export default DisplayContainer
